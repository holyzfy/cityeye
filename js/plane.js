'use strict';

(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;

function getPlane(resolve, reject) {
    var options = {
        name: 'plane',
        template: 'templates/plane.html',
        data: function () {
            return {
                // 'default', 'camera', 'map'
                view: 'default'
            };
        },
        mixins: [
            common.updateCanvasSize,
            app.pen
        ],
        mounted: function () {
            this.enablePen = true;
            drawMap(this.$refs.map);
        }
    };
    asyncComponent(options, resolve, reject);
}

function drawMap(wrap) {
    var map = new BMap.Map(wrap);
    map.centerAndZoom(new BMap.Point(116.404, 39.915), 16);
}

app.components.getPlane = getPlane;

})(app);
