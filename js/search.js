'use strict';

(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;

function getSearch(resolve, reject) {
    var options = {
        name: 'search',
        template: 'templates/search.html',
        data: function () {
            return {
                expand: false,
                keyword: '',
                accident: [],
                list: []
            };
        },
        mixins: [common.smoothScroll],
        watch: {
            expand: function (value) {
                value && this.search();
            }
        },
        methods: {
            search: function () {
                search(this);
            },
            toggleDropdown: function (item) {
                this.$set(item, 'expand', !item.expand);
            }
        }
    };
    asyncComponent(options, resolve, reject);
}

function search(context) {
    $.getJSON(common.urlMap.search, {
        keyword: context.keyword
    }, function (result) {
        if(result.status !== 0) {
            return;
        }
        var data = result.data;
        context.accident = data.accident || [];
        context.list = data.list || [];
    });
}

app.components.getSearch = getSearch;

})(app);
