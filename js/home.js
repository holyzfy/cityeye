'use strict';

(function (app) {

var common = app.common;
var components = app.components;
var asyncComponent = common.asyncComponent;

function getHome(resolve, reject) {
    var options = {
        name: 'home',
        template: 'templates/home.html',
        data: function () {
            return {};
        },
        components: {
            search: components.getSearch,
            'video-list': components.getVideoList
        },
        mounted: function () {
            demoMap();
        }
    };
    asyncComponent(options, resolve, reject);
}

function demoMap() {
    var map = new BMap.Map('map');
    map.centerAndZoom(new BMap.Point(116.404, 39.915), 16);
    map.addControl(new BMap.MapTypeControl());
    map.setCurrentCity('北京');
    map.enableScrollWheelZoom(true);

    (function () {
        var pt = new BMap.Point(116.407, 39.915);
        var myIcon = new BMap.Icon('images/marker.svg', new BMap.Size(97, 130), {
            imageSize: new BMap.Size(48.5, 65)
        });
        var marker = new BMap.Marker(pt, {icon: myIcon});

        var label = new BMap.Label('9', {offset: new BMap.Size(0, 13)});
        label.setStyle({
            width: '48.5px',
            color: '#287FFF',
            'font-size': '1.29rem',
            border: 0,
            background: 'none',
            'text-align': 'center'
        });
        marker.setLabel(label);

        map.addOverlay(marker);
    })();

    (function () {
        var pt = new BMap.Point(116.412, 39.920);
        var myIcon = new BMap.Icon('images/marker-accident.svg', new BMap.Size(130, 211), {
            imageSize: new BMap.Size(65, 105.5)
        });
        var marker = new BMap.Marker(pt, {icon: myIcon});
        map.addOverlay(marker);
    })();

    (function () {
        var pt = new BMap.Point(116.400, 39.910);
        var myIcon = new BMap.Icon('images/marker-car.svg', new BMap.Size(130, 211), {
            imageSize: new BMap.Size(65, 105.5)
        });
        var marker = new BMap.Marker(pt, {icon: myIcon});
        map.addOverlay(marker);
    })();
}

app.components.getHome = getHome;

})(app);
