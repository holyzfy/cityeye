'use strict';

(function (app) {

var common = app.common;
    
app.share = function (src, text) {
    var options = {
        template: 'templates/share.html',
        data: function () {
            return {
                show: true,
                src: src,
                text: text,
                keyword: '',
                list: [],
                checked: []
            };
        },
        watch: {
            keyword: function () {
                search(this);
            }
        },
        created: function () {
            search(this);
        },
        methods: {
            toggle: function (item) {
                this.$set(item, 'expand', !item.expand);
            },
            submit: function () {
                /*eslint-disable no-alert*/
                alert('分享给：\n' + this.checked);
                this.show = false;
            }
        }
    };
    common.mountDialog(options);
};

function search(context) {
    $.getJSON(common.urlMap.users, {
        keyword: context.keyword
    }, function (result) {
        if(result.status !== 0) {
            return;
        }
        context.list = result.data || [];
    });
}

})(app);

