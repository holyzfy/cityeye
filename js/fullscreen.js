'use strict';

(function (app) {

var fullscreen = {
    computed: {
        fullscreenElementName: function () {
            return getSuppportFnName([
                'fullscreenElement',
                'webkitFullscreenElement',
                'mozFullScreenElement'
            ]);
        },
        requestFullscreenName: function () {
            return getSuppportFnName([
                'requestFullscreen',
                'webkitRequestFullscreen',
                'mozRequestFullScreen'
            ]);
        },
        onfullscreenchangeName: function () {
            return getSuppportFnName([
                'onfullscreenchange',
                'onwebkitfullscreenchange',
                'onmozfullscreenchange'
            ]);
        },
        exitFullscreenName: function () {
            return getSuppportFnName([
                'exitFullscreen',
                'webkitExitFullscreen',
                'mozCancelFullScreen'
            ]);
        }
    },
    methods: {
        requestFullscreen: function (event) {
            var el = $(event.target).closest('.live-video')[0];
            el[this.requestFullscreenName]();
        },
        exitFullscreen: function () {
            document[this.exitFullscreenName](); 
        }
    }
};

function getSuppportFnName(list) {
    var undef;
    for(var index in list) {
        var name = list[index];
        var fn = document[name] !== undef ? document[name] : document.documentElement[name];
        if(fn !== undef) {
            return name;
        }
    }
}

app.fullscreen = fullscreen;

})(app);
