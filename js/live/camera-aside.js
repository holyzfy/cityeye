'use strict';

(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;

function getCameraAside(resolve, reject) {
    var options = {
        name: 'camera-aside',
        template: 'templates/live/camera-aside.html',
        data: function () {
            return {
                video: null,
                selectedId: -1
            };
        },
        activated: function () {
            this.setVideo(this.video || {id: '1'});
        },
        methods: {
            setVideo: function (data) {
                this.video = data;
                this.selectedId = data.id;
                this.$emit('video', data);
            },
            setHeadVideo: function () {
                var data =  {
                    id: 'head', 
                    compass: {
                        type: 'head',
                        rotate: 30
                    }
                };
                this.setVideo(data);
            },
            setTailVideo: function () {
                var data =  {
                    id: 'tail', 
                    compass: {
                        type: 'tail',
                        rotate: -120
                    },
                    pole: true
                };
                this.setVideo(data);
            }
        }
    };
     
    asyncComponent(options, resolve, reject);
}

app.components.getCameraAside = getCameraAside;

})(app);
