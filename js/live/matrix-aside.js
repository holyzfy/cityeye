'use strict';

(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;

function getMatrixAside(resolve, reject) {
    var options = {
        name: 'matrix-aside',
        template: 'templates/live/matrix-aside.html',
        data: function () {
            return {
                video: null,
                selectedId: -1,
                list: []
            };
        },
        mixins: [
            common.smoothScroll
        ],
        activated: function () {
            var context = this;
            if(context.video) {
                return context.setVideo(context.video);
            }
            getList(context, function (list) {
                context.setVideo(list[0]);
            });
        },
        methods: {
            toggleItem: function (item) {
                this.$set(item, 'expand', !item.expand);
            },
            setVideo: function (data) {
                data.disableCtrl = true;
                this.video = data;
                this.selectedId = data.id;
                this.$emit('video', data);
            }
        }
    };
     
    asyncComponent(options, resolve, reject);
}

function getList(context, callback) {
    callback = callback || $.noop;
    $.getJSON(common.urlMap.matrixvideo, function (result) {
        if(result.status !== 0) {
            return;
        }

        context.list = result.data;
        callback(result.data);
    });
}

app.components.getMatrixAside = getMatrixAside;

})(app);
