'use strict';
/* eslint-disable no-console */
(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;

function getLiveVideo(resolve, reject) {
    var options = {
        name: 'live-video',
        template: 'templates/live/video.html',
        props: ['video'],
        data: function () {
            return {
                moved: false
            };
        },
        mixins: [
            common.updateCanvasSize,
            app.pen,
            app.ctrl,
            app.fullscreen
        ],
        directives: {
            play: function (el, binding) {
                var changed = !binding.oldValue || (binding.value.id !== binding.oldValue.id);
                changed && console.log('play', el, binding.value);
            }
        }
    };
    asyncComponent(options, resolve, reject);
}

app.components.getLiveVideo = getLiveVideo;

})(app);
