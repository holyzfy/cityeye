'use strict';

(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;
var components = app.components;

function getLive(resolve, reject) {
    var options = {
        name: 'live',
        template: 'templates/live/index.html',
        data: function () {
            return  {
                asideSelected: true,

                // 'camera' or 'matrix'
                tab: 'camera', 
                liveMainSelected: true,
                video: null
            };
        },
        mixins: [
            app.fullscreen
        ],
        components: {
            cameraAside: components.getCameraAside,
            matrixAside: components.getMatrixAside,
            liveVideo: components.getLiveVideo
        },
        methods: {
            setVideo: function (data) {
                this.video = data;
            }
        }
    };

    asyncComponent(options, resolve, reject);
}

app.components.getLive = getLive;

})(app);
