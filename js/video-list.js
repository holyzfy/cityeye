'use strict';

(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;

function getVideoList(resolve, reject) {
    var instance = app.instance;
    var options = {
        name: 'video-list',
        template: 'templates/video-list.html',
        data: function () {
            return {
                expand: false,
                list: [],
                selectedIndex: -1,
                showButtonTop: false
            };
        },
        mixins: [
            common.smoothScroll,
            common.updateCanvasSize
        ],
        created: function () {
            var context = this;
            var unwatch = context.$watch('expand', function () {
                unwatch();
                getList(context);
            });
        },
        watch: {
            selectedIndex: function (value) {
                var item = this.$refs['video-item-' + value];
                item && item[0].scrollIntoView();
            }
        },
        methods: {
            togglePlay: function (item) {
                this.$set(item, 'playing', !item.playing);
            },
            scrollTop: function () {
                var wrap = this.$refs['video-list-inner'];
                $(wrap).animate({
                    scrollTop: 0
                });
            },
            scroll: (function () {
                var scrollTimer;
                return function () {
                    var context = this;
                    var wrap = context.$refs['video-list-inner'];
                    clearTimeout(scrollTimer);
                    scrollTimer = setTimeout(function () {
                        context.showButtonTop = $(wrap).scrollTop() > 0;
                    }, 100);
                };
            })(),
            fullscreen: function () {
                instance.video = {};
            },
            snapshoot: function () {
                instance.video = {
                    action: 'snapshoot',
                    picture: {/*数据部分*/}
                };
            },
            share: function (item) {
                var src = 'http://pic-bucket.nosdn.127.net/photo/0025/2017-07-19/CPMNSGK20BGT0025NOS.jpeg';
                var text = item.address;
                app.share(src, text);
            }
        }
    };
    asyncComponent(options, resolve, reject);
}

function getList(context) {
    $.getJSON(common.urlMap.camera, function (result) {
        if(result.status !== 0) {
            return;
        }
        context.list = result.data;
    });
}

app.components.getVideoList = getVideoList;

})(app);
