'use strict';
/* eslint-disable no-console */
(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;

function getFullscreenVideo(resolve, reject) {
    var options = {
        name: 'fullscreen-video',
        template: 'templates/fullscreen-video.html',
        props: ['video'],
        mixins: [
            common.updateCanvasSize,
            app.pen,
            app.ctrl
        ],
        methods: {
            zoomIn: function () {
                console.log('放大');
            },
            zoomOut: function () {
                console.log('缩小');
            },
            direction: function (type) {
                console.log('方向：', type);
            }
        }
    };
    asyncComponent(options, resolve, reject);
}

app.components.getFullscreenVideo = getFullscreenVideo;

})(app);
