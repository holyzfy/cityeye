'use strict';

(function (app, ZingTouch) {
/* eslint-disable no-console */

var common = app.common;

var ctrl = {
    data: function () {
        return {
            moved: false
        };
    },
    created: function () {
        var context = this;
        context.$on('move', function (left, top) {
            if(context.video.disableCtrl) {
                return;
            }
            context.moved = true;
            // left > 0 右
            // left < 0 左
            // top > 0 下
            // top < 0 上
            console.info('move', left, top);
        });
    },
    mounted: function () {
        detectGesture(this);
    },
    watch: {
        video: function (value) {
            if(!value) {
                return;
            }
            detectGesture(this);
            this.moved = false;
        }
    },
    methods: {
        resetPosition: function () {
            this.moved = false;
            console.log('TODO reset position');
        },
        zoomIn: function (event) {
            console.log('放大', event);
        },
        zoomOut: function (event) {
            console.log('缩小', event);
        },
        zoom: common.throttle(function (event) {
            var context = this;
            if(context.drawExpand || context.video.disableCtrl) {
                return;
            }
            var deltaY = event.deltaY;
            var action = deltaY < 0 ? 'zoomIn' : 'zoomOut';
            if(Math.abs(deltaY) > 0) {
                context[action](event);
            }
        }, 80)
    }
};

function detectGesture(context) {
    context.$nextTick(function () {
        var canvas = context.$refs['pen-canvas'];
        if(!canvas) {
            return;
        }
        var region = new ZingTouch.Region(canvas);
        region.bind(canvas, 'pinch', function (event) {
            if(context.drawExpand || context.enablePen) {
                return;
            }
            context.zoomOut(event);
        }); 
        region.bind(canvas, 'expand', function (event) {
            if(context.drawExpand || context.enablePen) {
                return;
            }
            context.zoomIn(event);
        }); 
    });
}

app.ctrl = ctrl;

})(app, window.ZingTouch);
