'use strict';

(function (app) {

var components = app.components;

app.getRoutes = function () {
    return [
        {
            name: 'home',
            path: '/',
            component: components.getHome
        },
        {
            name: 'cameraview',
            path: '/cameraview',
            component: components.getCameraView
        },
        {
            name: 'matrixvideo',
            path: '/matrixvideo',
            component: components.getMatrixVideo
        },
        {
            name: 'live',
            path: '/live',
            component: components.getLive
        },
        {
            name: 'plane',
            path: '/plane',
            component: components.getPlane
        }
    ];
};

})(app);
