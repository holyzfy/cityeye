'use strict';

(function (app) {

var components = app.components;

function start() {
    var router = new VueRouter({
        routes: app.getRoutes()
    });

    router.afterEach(function (to, from) {
        if(to.path !== from.path && app.dialog) {
            app.dialog.show = false;
        }
    });

    app.instance = new Vue({
        el: '#app',
        router: router,
        data: function () {
            return {
                video: null
            };
        },
        components: {
            'fullscreen-video': components.getFullscreenVideo
        },
        methods: {
            exitFullScreen: function () {
                this.video = null;
            }
        }
    });
}

app.start = start;

})(app);
